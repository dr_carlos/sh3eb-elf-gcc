# sh3eb-elf-gcc Arch Linux PKGBUILD

An Arch Linux PKGBUILD for building GCC with the sh3eb (SuperH 3 Big Endian) target, used in Casio Prizms and possibly other Casio calculators.

## Usage

You can git clone this repo with `git clone https://gitlab.com/dr_carlos/sh3eb-elf-gcc`, cd into it, and run `makepkg -si` to build the repo.

You could also clone the AUR package 'sh3eb-elf-gcc' using `git clone https://aur.archlinux.org/sh3eb-elf-gcc` and patch it using `patch --strip=1 < aur.patch`, using the aur.patch file available here, or just change the PKGBUILD based on this aur.patch file.
